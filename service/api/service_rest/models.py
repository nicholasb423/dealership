from django.db import models
from django.urls import reverse



class Technician(models.Model):
    first_name = models.CharField(max_length=40)
    last_name = models.CharField(max_length=40)
    employee_id = models.CharField(max_length=80)

    def get_api_url(self):
        return reverse("api_show_technician", kwargs={"id": self.id})


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)

    def __str__(self):
        return self.vin


class Appointment(models.Model):
    date_time = models.DateTimeField(null=True)
    reason = models.TextField()
    status = models.CharField(max_length=40, choices=(
        ("cancel", "Cancel"),
        ("finished", "finished"),
        ("created", "Created"),
    ))
    vin = models.CharField(max_length=17)
    customer =models.CharField(max_length=150)
    technician = models.ForeignKey(
        Technician,
        related_name="appointment",
        on_delete=models.PROTECT,
    )
    vip = models.BooleanField(default=False)

    def __str__(self):
        return self.vin

    def get_api_url(self):
        return reverse("api_show_appointment", kwargs={"id": self.id})
