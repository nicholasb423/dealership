from django.urls import path
from .views import (api_list_technicians,
                    api_list_appointments,
                    api_show_technician,
                    api_show_appointment,
                    appointment_finish,
                    appointment_cancel,
                    )

urlpatterns = [
    path("technicians/", api_list_technicians, name="list_technician"),
    path("technicians/", api_list_technicians, name="create_technician"),
    path("appointments/", api_list_appointments, name="list_appointments"),
    path("appointments/", api_list_appointments, name="create_appointment"),
    path("technician/<int:id>/", api_show_technician, name="show_technician"),
    path("appointment/<int:id>/", api_show_appointment, name="show_appointment"),
    path("appointment/<int:id>/finish", appointment_finish, name="appointment_finish"),
    path("appointment/<int:id>/cancel", appointment_cancel, name="appointment_cancel"),
]
