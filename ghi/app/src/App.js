import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import SalespersonForm from './sales/SalespersonForm';
import SalespersonList from './sales/SalespersonList';
import CustomerForm from './sales/CustomerForm';
import CustomerList from './sales/CustomerList';
import SaleForm from './sales/SaleForm';
import TechnicianForm from './Service/TechnicianForm';
import TechnicianList from './Service/TechnicianList';
import AppointmentForm from './Service/AppointmentForm';
import AppointmentList from './Service/AppointmentList';
import ServiceHistory from './Service/ServiceHistory';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="technicians">
            <Route path="new" element={<TechnicianForm />} />
            <Route path="list" element={<TechnicianList />} />
          </Route>
          <Route path="appointments">
            <Route path="new" element={<AppointmentForm />} />
            <Route path="list" element={<AppointmentList />} />
          </Route>
          <Route path="serviceHistory" element={<ServiceHistory />} />
          <Route path="salesperson" element={<SalespersonForm />} />
          <Route path="salespersons" element={<SalespersonList />} />
          <Route path="customer" element={<CustomerForm />} />
          <Route path="customers" element={<CustomerList />} />
          <Route path="saleform" element={<SaleForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
