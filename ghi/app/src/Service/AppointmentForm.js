import React, {useEffect, useState} from 'react';


function AppointmentForm() {
    const [vin, setVin] = useState('');
    const [customer, setCustomer] = useState('');
    const [date, setDate] = useState('');
    const [time, setTime] = useState('');
    const [technicians, setTechnicians] = useState([]);
    const [technician, setTechnician] = useState('');
    const [reason, setReason] = useState('');

    const handleVinChange = (event) => {
        const value = event.target.value;
        setVin(value);
    }

    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustomer(value);
    }

    const handleDateChange = (event) => {
        const value = event.target.value;
        setDate(value);
    }

    const handleTimeChange = (event) => {
        const value = event.target.value;
        setTime(value);
    }

    const handleTechnicianChange = (event) => {
        const value = event.target.value;
        setTechnician(value);
    }

    const handleReasonChange = (event) => {
        const value = event.target.value;
        setReason(value);
    }

    const fetchData = async () => {
        const url = 'http://localhost:8080/api/technicians/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.technicians);
        }
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.vin = vin;
        data.customer = customer;
        data.date_time = `${date} ${time}`;
        data.technician = technician;
        data.reason = reason;

        const appointmentUrl = 'http://localhost:8080/api/appointments/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            Headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(appointmentUrl, fetchConfig);
        if (response.ok) {
            setVin('');
            setCustomer('');
            setDate('');
            setTime('');
            setTechnician('');
            setReason('');
        }
    }

    useEffect(() => {
        fetchData();
    }, []);
    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a service appointment</h1>
            <form onSubmit={handleSubmit} id="create-service-appointment-form">
              <div className="form-floating mb-3">
                <input onChange={handleVinChange} placeholder="Automoblie VIN" required type="text" name="vin" value={vin} id="vin" className="form-control"/>
                <label htmlFor="vin">Automobile VIN</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleCustomerChange} placeholder="Customer" required type="text" name="customer" value={customer} id="customer" className="form-control"/>
                <label htmlFor="customer">Customer</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleDateChange} placeholder="Date" required type="date" name="date" value={date} id="date" className="form-control"/>
                <label htmlFor="date">Date</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleTimeChange} placeholder="Time" required type="time" name="time" value={time} id="time" className="form-control"/>
                <label htmlFor="time">Time</label>
              </div>
              <div className="mb-3">
                    <select onChange={handleTechnicianChange} required name="technician" id="technician" className="form-select">
                    <option value="">Technician</option>
                    {technicians.map(technician => {
                        return (
                            <option key={technician.employee_id} value={technician.employee_id}>
                                {technician.first_name + " " + technician.last_name}
                            </option>
                        )
                    })}
                    </select>
                </div>
                <div className="form-floating mb-3">
                <input onChange={handleReasonChange} placeholder="Reason" required type="text" name="reason" value={reason} id="reason" className="form-control"/>
                <label htmlFor="reason">Reason</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
}

export default AppointmentForm;
