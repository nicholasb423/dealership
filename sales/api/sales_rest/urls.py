from django.urls import path
from .views import (
    api_list_salespersons,
    api_salesperson,
    api_list_customers,
    api_customer,
    api_saleslist,
    api_sale,
)

urlpatterns = [
    path(
        "salespeople/",
        api_list_salespersons,
        name = "api_salespersons"
    ),
    path(
        "salespeople/<int:employee_id>/",
        api_salesperson,
        name="api_salesperson",
    ),
    path(
        "customers/",
        api_list_customers,
        name = "api_customers",
    ),
    path(
        "customers/<int:pk>/",
        api_customer,
        name="api_customer",
    ),
    path(
        "sales/",
        api_saleslist,
        name="api_sales",
    ),
    path(
        "sales/<int:pk>",
        api_sale,
        name="api_sale",
    ),
]
