from django.db import models
from django.urls import reverse

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)

    def __str__(self) -> str:
        return self.vin
    def get_api_url(self):
        return reverse("api_automobile", kwargs={"vin": self.vin})

class Salesperson(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.PositiveIntegerField(primary_key=True)

    def get_api_url(self):
        return reverse("api_salesperson", kwargs={"employee_id": self.employee_id})


class Customer(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    address = models.CharField(max_length=200)
    phone_number = models.PositiveBigIntegerField(default=0)

    def get_api_url(self):
        return reverse("api_customer", kwargs={"pk": self.phone_number})


class Sale(models.Model):
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="automobile",
        on_delete = models.PROTECT
    )
    salesperson = models.ForeignKey(
        Salesperson,
        related_name="sales_person",
        on_delete= models.PROTECT
    )
    customer = models.ForeignKey(
        Customer,
        related_name="customer",
        on_delete = models.PROTECT
    )
    price = models.PositiveBigIntegerField(default=0)

    def get_api_url(self):
        return reverse("api_customer", kwargs={"pk": self.price})
