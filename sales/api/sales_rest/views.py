from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from .encoders import (
    SaleListEncoder,
    CustomerListEncoder,
    SalespersonListEnconder,
)
from .models import Salesperson, Customer, Sale, AutomobileVO


@require_http_methods(["GET", "POST"])
def api_list_salespersons(request):
    if request.method == "GET":
        salespersons = Salesperson.objects.all()
        return JsonResponse(
            {"salespersons": salespersons},
            encoder=SalespersonListEnconder,
        )
    else:
        try:
            content = json.loads(request.body)
            salesperson = Salesperson.objects.create(**content)
            return JsonResponse(
                salesperson,
                encoder=SalespersonListEnconder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the salesperson"}
            )
            response.status_code = 400
            return response

require_http_methods(["DELETE"])
def api_salesperson(request, employee_id):
    if request.method == "DELETE":
        try:
            salesperson = Salesperson.objects.get(employee_id=employee_id)
            salesperson.delete()
            return JsonResponse(
                salesperson,
                encoder=SalespersonListEnconder,
                safe=False,
            )
        except Salesperson.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})

require_http_methods(["GET, POST"])
def api_list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerListEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerListEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create a Customer"}
            )
            response.status_code = 400
            return response

require_http_methods(["DELETE"])
def api_customer(request, pk):
    if request.method == "DELETE":
        try:
            customer = Customer.objects.get(id=pk)
            customer.delete()
            return JsonResponse(
                customer,
                encoder=CustomerListEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})

require_http_methods(["GET", "POST"])
def api_saleslist(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SaleListEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            vin_id = content["automobile"]
            vin = AutomobileVO.objects.get(vin=vin_id)
            content["automobile"] = vin
            saleperson_id = content["salesperson"]
            saleperson = Salesperson.objects.get(employee_id=saleperson_id)
            content["salesperson"] = saleperson
            customer_id = content["customer"]
            customer = Customer.objects.get(phone_number=customer_id)
            content["customer"] = customer
            sale = Sale.objects.create(**content)
            return JsonResponse(
                sale,
                encoder=SaleListEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the sale"}
            )
            response.status_code = 400
            return response

require_http_methods(["DELETE"])
def api_sale(request, pk):
    if request.method == "DELETE":
            try:
                sale = Sale.objects.get(id=pk)
                sale.delete()
                return JsonResponse(
                    sale,
                    encoder=SaleListEncoder,
                    safe=False,
                )
            except Customer.DoesNotExist:
                return JsonResponse({"message": "Does not exist"})
